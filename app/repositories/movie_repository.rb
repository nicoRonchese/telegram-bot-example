class MovieRepository
  require_relative '../../models/movie'
  include Faraday
  API_KEY = ENV['OMDB_API_KEY']
  API_URL = 'http://www.omdbapi.com/'.freeze

  def fetch_movie(title)
    response = Faraday.get(API_URL, { t: title, apikey: API_KEY })
    JSON.parse(response.body)
  end

  def find_by_title(title)
    movie_hash = fetch_movie(title)
    if movie_hash['Response'] == 'True'
      Movie.new(movie_hash['Title'], movie_hash['Awards'])
    else
      raise MovieNotFound
    end
  end
end

class MovieNotFound < StandardError; end
