require 'spec_helper'
require_relative '../app/repositories/movie_repository'

describe MovieRepository do
  let(:repository) { described_class.new }

  def stub_omdb(title, api_omdb_response_body)
    stub_request(:get, "http://www.omdbapi.com/?apikey&t=#{title}")
      .with(
        headers: {
          'Accept' => '*/*',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'User-Agent' => 'Faraday v2.7.4'
        }
      )
      .to_return(status: 200, body: api_omdb_response_body.to_json, headers: {})
  end

  it 'should find movie by title' do
    title = 'Titanic'
    api_omdb_response_body = { "Title": 'Titanic', "Awards": 'Won 11 Oscars. 126 wins & 83 nominations total', "Response": 'True' }
    stub_omdb(title, api_omdb_response_body)

    found_user = repository.find_by_title('Titanic')

    expect(found_user.title).to eq 'Titanic'
  end

  it 'if movie can not be found raise error' do
    api_omdb_response_body = { "Response": 'False', "Error": 'Movie not found!' }
    stub_omdb('Nicobot', api_omdb_response_body)
    expect do
      repository.find_by_title('Nicobot')
    end.to raise_error(MovieNotFound)
  end
end
