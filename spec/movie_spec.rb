require 'spec_helper'
require_relative '../models/movie'

describe Movie do
  it 'Movie should store it title' do
    movie = described_class.new('hola', 'awards')
    expect(movie.title).to eq 'hola'
  end

  it 'Movie should store its awards' do
    movie = described_class.new('hola', 'awards')
    expect(movie.awards).to eq 'awards'
  end

  it 'Movie know if it has awards' do
    movie = described_class.new('hola', 'N/A')
    expect(movie.has_awards?).to eq false
  end
end
